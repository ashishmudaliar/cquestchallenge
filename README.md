# cquestchallenge

## Description
Simple Web App made with  [Flask](https://flask.palletsprojects.com/en/2.0.x/) which starts a `wsgi` server and returns `Hello, World!` from the default GET endpoint


## Usage
This project utilizes Docker to setup and run the application.

- Run `docker_build.sh` to build the Docker container.
- Run `docker_run.sh` to run the Docker container.

The app will the available on [localhost](http://localhost:5000)

## Tests
The file `test_app.py` contains a Test case to test the API

## CI/CD
The application utilizes a Gitlab runner installed on an AWS instance to implement continuous integration. The CI runs on every commit to the main branch.
The configuration for the CI can be found in the `.gitlab-ci.yml` file.
